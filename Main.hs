{-#LANGUAGE OverloadedStrings#-}
module Main
where

import SiteBuilder.Conf (loadConf, Conf)
import SiteBuilder.Editor
import SiteBuilder.Site (emptySite)
import System.IO

main = do
    confE <- loadConf "config.yml"
    either bail go confE
    where
        bail :: String -> IO ()
        bail = hPutStrLn stderr
        go :: Conf -> IO ()
        go = runEditor emptySite
