module SiteBuilder.VariantUtils
where

import Data.Variant
import Data.Variant.ToFrom
import Prelude hiding (lookup, toInteger)
import Data.List (sortBy)
import qualified Data.Map as Map
import Data.Map (Map)

pairwise :: (a -> b) -> (c -> d) -> (a, c) -> (b, d)
pairwise fstF sndF (a, b) = (fstF a, sndF b)

-- | AList cons on variants / ToVariants
infixr 5 #:
(#:) :: (ToVariant a, ToVariant b, ToVariant c) => (a, b) -> c -> Variant
(k, v) #: xs =
    AList $ (toVariant k, toVariant v):(toAList $ toVariant xs)

instance (ToVariant a, ToVariant b) => ToVariant (Map a b) where
    toVariant = toVariant . map (pairwise toVariant toVariant) . Map.toList

instance (Ord a, FromVariant a, FromVariant b) => FromVariant (Map a b) where
    fromVariant = Map.fromList . map (pairwise fromVariant fromVariant) . toAList

isList :: Variant -> Bool
isList (List _) = True
isList (AList _) = True
isList _ = False

isAList :: Variant -> Bool
isAList (AList _) = True
isAList _ = False

isScalar :: Variant -> Bool
isScalar (Function _) = False
isScalar (List _) = False
isScalar (AList _) = False
isScalar _ = True

variantKeySort :: Variant -> Variant -> Variant
variantKeySort = variantKeySorts id

variantNumKeySort :: Variant -> Variant -> Variant
variantNumKeySort = variantKeySorts toInteger

variantKeySorts :: Ord a => (Variant -> a) -> Variant -> Variant -> Variant
variantKeySorts proj key =
    List . sortBy cmp . values
    where
        cmp :: Variant -> Variant -> Ordering
        cmp x y = compare (proj $ lookup key x) (proj $ lookup key y)

variantMap :: Variant -> Variant -> Variant
variantMap fv lv =
    let Function f = fv
        f' (k, v) = f [v]
    in List (vamap f' lv)
