module SiteBuilder.Editor
( runEditor
)
where

import SiteBuilder.Site
import SiteBuilder.Conf (Conf, editorPort)
import Happstack.Server hiding (Conf)
import Control.Monad (msum)
import Control.Monad.IO.Class (liftIO)
import Data.IORef

runEditor :: Site -> Conf -> IO ()
runEditor site sbconf = do
    let serverPort = editorPort sbconf
    putStrLn $ "Starting server on port " ++ show serverPort ++ "..."
    siteRef <- newIORef site
    simpleHTTP (nullConf { port = serverPort }) (editRoutes siteRef)

editRoutes siteRef =
    msum
        [ nullDir >> startPageR siteRef
        , notFound "No Such Page Dude"
        ]

startPageR siteRef = do
    site <- liftIO $ readIORef siteRef
    ok $ show site
