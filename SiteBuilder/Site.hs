{-#LANGUAGE TemplateHaskell #-}
module SiteBuilder.Site
where

import Data.Variant
import Data.Aeson (ToJSON, toJSON)
import SiteBuilder.VariantJSON
import SiteBuilder.VariantUtils
import Control.Lens

data ContentItem =
    ContentItem
        { _ciID :: Int
        , _ciSlug :: String
        , _ciData :: Variant
        }
        deriving (Show)
$(makeLenses ''ContentItem)

instance ToJSON ContentItem where
    toJSON (ContentItem id slug d) =
        toJSON $ ("id", id) #: ("slug", slug) #: d

data Site =
    Site
        { _siteContent :: [ ContentItem ]
        }
        deriving (Show)

emptySite :: Site
emptySite = Site []

$(makeLenses ''Site)
