module SiteBuilder.VariantJSON
where

import Data.Aeson (toJSON, ToJSON, FromJSON)
import Data.Variant
import qualified Data.Map as Map

instance ToJSON Variant where
    toJSON Null = toJSON (Nothing :: Maybe String)
    toJSON (Integer i) = toJSON i
    toJSON (Double d) = toJSON d
    toJSON (String s) = toJSON s
    toJSON (Bool b) = toJSON b
    toJSON (List xs) = toJSON xs
    toJSON (AList xs) = toJSON (Map.fromList [ (flatten k, v) | (k, v) <- xs ])
    toJSON (Function _) = toJSON "<<function>>"
