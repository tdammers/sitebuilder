{-#LANGUAGE TemplateHaskell #-}
module SiteBuilder.Conf
( Conf (..)
, loadConf
)
where

import Data.Aeson
import Data.Aeson.TH
import Data.Yaml

data Conf =
    Conf
        { editorPort :: Int
        , previewPort :: Int
        }

$(deriveJSON defaultOptions ''Conf)

loadConf :: FilePath -> IO (Either String Conf)
loadConf fp =
    handleParseException =<< decodeFileEither fp
    where
        handleParseException :: Either ParseException Conf -> IO (Either String Conf)
        handleParseException (Left err) = return $ Left $ show err
        handleParseException (Right conf) = return $ Right conf
